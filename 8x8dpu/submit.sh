#!/bin/bash -l
#sbatch -N 2 -w thor0[25-32],thor-bf[25-32] --ntasks-per-node=1 ../run.slurm
sbatch -N 16 -w thor[0-5],thor-bf[25-32] --ntasks-per-node=8 run.slurm
#sbatch -N 4 -w thor[0-5],thor-bf[25-32] --ntasks-per-node=8 run.slurm