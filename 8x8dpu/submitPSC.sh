#!/bin/bash
#SBATCH -N 4
#SBATCH -p RM
#SBATCH --ntasks-per-node=8
#SBATCH -t 00:20:00
#SBATCH --job-name=xcompact3d
#SBATCH --output=log.txt


# cd $SLURM_SUBMIT_DIR
# Load MPI module
mpirun -np 8 ../xcompact3d
# or "srun ./mpi_example"